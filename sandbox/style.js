module.exports = function(){
 return JSON.stringify(`
 





.plw-clearfix:after {
  visibility: hidden;
  display: block;
  font-size: 0;
  content: " ";
  clear: both;
  height: 0;
}
.plw-clearfix { display: inline-block; }
/* start commented backslash hack \*/
* html .clearfix { height: 1%; }
.plw-clearfix { display: block; }
/* close commented backslash hack */
/**start from here **/


.plw-container {
  background-color: #0A57A4;
  width:100%;
  text-align: center;
  font-style:normal;
}

/* Banner */
.plw-img-banner{
   width:100%;
}
.plw-img-banner img{
   max-width: 0%;
   vertical-align:middle;
}

.plw-description-banner{
  color: white;
  text-align: center;
  font-style:normal;
  width:100%;
  padding-top:1em;
}

  #plw-op-banner .plw-description-banner{
    width:100%;
    padding-right: 1em;
    padding-left: 1em;
  }

  #plw-op-banner h1{
    text-align:center;
    font-size:18px;
    color:#ffb515;
    margin-top:0;
    margin-bottom:0;
  }

  #plw-op-banner p{
    text-align:center;
    font-size:16px;
  }


#plw-hasNoPloan-banner .plw-description-banner {
    text-align: center;
    padding-left: .5em;
    padding-right: .5em;
    padding-top: 1em;
    padding-bottom: .5em;
}

#plw-hasNoPloan-banner .plw-description-banner p {
    color: #ffB515;
    margin-top: 0;
    margin-bottom: 0;
    font-weight: bold;
    font-family: klavika;
}

#plw-hasHadPloan-banner .plw-description-banner {
    text-align: center;
    padding-left: .5em;
    padding-right: .5em;
    padding-top: 1em;
    padding-bottom: .5em;
}

#plw-hasHadPloan-banner .plw-description-banner p {
    color: #ffB515;
    margin-top: 0;
    margin-bottom: 0;
    font-weight: bold;
    font-family: klavika;
}
#plw-hadploan-accountlist {
    background-color:white;
}

#plw-hadploan-accountlist h2{
     color:black;
}

.plw-link-content-banner{
    padding-top: .5em;
    padding-bottom: 1em;
    width: 100%;
    font-size: 12px;
    font-weight: bold;
}

.plw-link-content-banner span{
  background : #ffB515;
  color:black;
  text-align:center; 
}
 

/* account list */
.plw-account-list-img{
  width: 100%;
}

.plw-account-list-img img{
  width: 100%;
  vertical-align:middle;
}

.plw-account-list-content {
    width: 100%;
    font-style:normal;
  }

.plw-account-list-content h1 {
    text-align: center;
    font-family: foro;
    color: white;
    font-weight: normal;
    font-size: 15px;
    margin-top: 0;
    margin-bottom: 0;
}

.plw-account-list-content p {
    color: #ffB515;
    margin-top: 0;
    margin-bottom: 0;
    padding-top: .5em;
    font-size: 16px;
    text-align: center;
    font-weight: bold;
    font-family: klavika;
}
}


.plw-account-list-link {
    padding-top: 0.2em;
    padding-bottom: .3em;
    width: 100%;
    color: #ffB515;
    font-weight: bold;
    font-size: 14px;
    font-style:normal;
    text-align: center;
}

.plw-account-list-link a{
    color: #ffB515;
    font-weight: bold;
    font-size: 16px;
    text-decoration:none;
}

#plw-hasHadPloan-account-list .plw-account-list-content{

    padding-top: .8em;
    padding-bottom: .5em;
    padding-left: 1em;
    padding-right: 1em;
}


#plw-hasHadPloan-account-list .plw-account-list-link {

  padding-bottom:1em;
}
#plw-hasHadPloan-account-list .plw-account-list-link a{
   padding-top: 0.5em;
    background: #ffB515;
    color: black;
    font-weight: bold;
    font-size: 12px;
    text-align: center;
  }

#plw-hasNoPloan-account-list .plw-account-list-content{

    padding-top: .8em;
    padding-bottom: .5em;
    padding-left: 1em;
    padding-right: 1em;
}


#plw-hasNoPloan-account-list .plw-account-list-link {

  padding-bottom:1em;
}
#plw-hasNoPloan-account-list .plw-account-list-link a{
   padding-top: 0.5em;
    background: #ffB515;
    color: black;
    font-weight: bold;
    font-size: 12px;
    text-align: center;
  }

#plw-openOpportunity-withLoanAccounts{
  padding:0;
}

#plw-hasNoPloan-withLoanAccounts{
  padding:0;
}

#plw-hasOrHadPloan-withLoanAccounts{
  padding:0;
}
/*oa-banner satrt*/

#plw-oa-banner{
  width:100%;
  margin-left:0;
}

#plw-oa-banner div{
   font-size:12px;
}

#plw-oa-banner .plw-account-list-content h1 {
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    margin-bottom: .5em;
    margin-top: .5em;
    width: 100%;
}

#plw-oa-banner .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
}

#plw-oa-banner .plw-account-list-content div{
  text-align:left;
  margin-bottom:.5em;
}
#plw-oa-banner .plw-account-list-link{
    padding-top: 1em;
    width: 100%;
    float: right;
    text-align: center;
}

#plw-oa-banner .plw-account-list-link a{
  color:white;
  background: #0A57A4;
  font-size: 16px;
}

/*oa -banner-end*/

/*oa-account-list start*/

#plw-openApplication-withLoanAccounts{
  padding:0;
}

#plw-oa-accountlist p{
    margin-top: 1em;
    margin-bottom: 1em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
    font-family: arial, sans-serif;
    font-weight: normal;
}

#plw-oa-accountlist div{
  font-size:12px;
}
#plw-oa-accountlist .plw-account-list-content{
  padding-left:.5em;
  padding-right:1em;
  padding-top: .8em;
  padding-bottom: .5em;
}

#plw-oa-accountlist a{
  font-weight:bold;
  font-size:16px;
  font-family:klavika;
}

#plw-oa-accountlist .plw-account-list-link a {
  color:white;
  font-style:normal;
}

#plw-oa-accountlist .plw-account-list-link{
  padding-top:1em;
  width:100%;
  text-align:center;
}
/*oa-account-list end*/

/* loan content block */

.plw-loan-content-img{

  width: 100%;
}

.plw-loan-content-img img{

  max-width: 100%;
  vertical-align: middle;
}

.plw-loan-content-content{
  width:100%;
  text-align:center;
  padding-top: 1em;
  padding-bottom: .5em;
  padding-left: 1em;
  padding-right: 1em;
}

.plw-loan-content-content h1{
    font-family: foro;
    color: white;
    font-weight: normal;
    font-size: 13px;  
    margin-top: 0;
    margin-bottom: 0;
}

.plw-loan-content-content p{
    color: #ffB515;
    margin-top: 0;
    margin-bottom: 0;
    padding-top: 1em;
    font-size: 16px;
    font-weight: bold;
    font-family: klavika;
}

#plw-loan-content-content-text{
  color:white;
  font-weight:normal;
  font-size: 14px;
  padding-top:1em;
  font-family:arial, sans-serif;
}
.plw-loan-content-link{

  padding-top: 1em;
    width: 100%;
    text-align: center;
    font-size:14px;
    padding-right: 1em;
    padding-left: 1em;
    padding-bottom:1em;
}

.plw-loan-content-link a{
  background:#ffB515;
  color:black;
}

#plw-openOpportunity-withNoLoanAccounts{
  padding:0;
}

#plw-hasNoPloan-withNoLoanAccounts{
  padding:0;
}

#plw-hasOrHadPloan-withNoLoanAccounts{
  padding:0;
}

/*oa-loancontent satrt*/

#plw-openApplication-withNoLoanAccounts{
  padding:0;
}

#plw-oa-loancontent{
  width:100%;
}

#plw-oa-loancontent div{
  font-size:12px;
}
#plw-oa-loancontent .plw-account-list-content {
    padding-left: .5em;
    width: 100%;
    float: left;
}

#plw-oa-loancontent .plw-account-list-content h1 {
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    margin-bottom: .5em;
    margin-top: .5em;
    width: 100%;
    font-family:klavika;
}

#plw-oa-loancontent .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
    font-family: arial, sans-serif;
    font-weight:normal;
}

#plw-oa-loancontent .plw-account-list-content div{
  text-align:left;
  margin-bottom:.5em;
}
#plw-oa-loancontent .plw-account-list-link{
    padding-top: 1em;
    width: 100%;
    float: right;
    text-align: center;
}

#plw-oa-loancontent .plw-account-list-link a{
  color:white;
  background:#0A57A4;
  font-size: 16px;
  font-style:normal;
}


/*oa -loancontent-end*/

/*op loan content block start*/

.plw-op-loancontent-description{
  width:100%;
  text-align:center;
}

.plw-op-loancontent-description h1{
  padding-top:.5em;
  padding-left:0em;
  font-family:foro;
  color:white;
  font-weight: normal;
  margin-top:0;
  margin-bottom:0;
  font-size:17px;
 }
 
 .plw-op-loancontent-description p{
  padding-top:.5em;
  padding-left:.5em;
  padding-right:.5em;
  color:#ffbf00;
  font-size:17px;
  font-weight: bold;
  font-family:klavika;
 }

 .plw-op-loancontent-link {
  width:100%;
  padding-top: 1em;
  text-align:center;
 }

 .plw-op-loancontent-link a{
  background:#ffbf00;
  color:black;
}

 .plw-op-loancontent-info {
    color: white;
    text-align: left;
    width: 100%;
    text-align:center;
    padding-left: 0.8em;
    border-left: none;
    border-top: none;
    margin-top: 0.4em;
    margin-bottom: 0.6em;
    border-top: 2px solid grey;
    width: 80%;
    display: inline-block;
    word-wrap: break-word;
 }
 .plw-op-loancontent-info h2{
  padding-top:.8em;
  font-family:foro;
  color:white;
  font-weight: normal;
  font-size: 16px;
 }

.plw-op-loancontent-info-top-pad{
  padding-top:.5em;
}

/*op loan content block end*/

/* Transaction history  */
 .plw-img-transac-feed{
  
    width: 0%;
    float: left;
  }

.plw-img-transac-feed img{
  max-width:100%;
  vertical-align:middle;
}


  #plw-op-transaction-feed img{
   max-width: 10%;
    padding-top: .5em;
  } 
  #plw-op-transaction-feed{
    padding-top:.5em;
    padding-bottom:.5em;
  }

 #plw-op-transaction-feed h1{
  font-size:16px;
  padding-top:.3em;
  color: #ffB515;
  margin-top:0;
  margin-bottom:0;
 }

 #plw-op-transaction-feed p{
  font-size:13px;
  color: white;
  font-weight: normal;
  font-family: arial, sans-serif;
 }
 

 .plw-description-transac-feed{

    width:100%;
    float:left;
  }

.plw-description-transac-feed p{
    color: #ffB515;
    padding-left: 1em;
    padding-top: 1em;
    padding-right: 1em;
    padding-bottom:.5em;
    text-align: center;
    font-size: 20px;
    font-weight:bold;
    font-family:klavika;

}

.plw-link-content-transac-feed{
    float:left;
    padding-top:.5em;
    padding-bottom:1em;
    text-align: center;
    font-size: 16px;
    color: #ffB515;
    font-weight: bold;
    padding-right: 1em;
    width: 100%;

}
.plw-link-content-transac-feed span{
  background: #ffB515;
  color:black;
}

#plw-op-transaction-feed .plw-link-content-transac-feed{
    font-size: 13px;
    padding-top:.5em;
}

/*oa-transactionfeed start*/

#plw-oa-transactionfeed{
  width:100%;
  margin-left:0;
}

#plw-oa-transactionfeed div{
  font-size:12px;
}
#plw-oa-transactionfeed .plw-account-list-content {
    padding-left: .5em;
    padding-top:0;
    width: 100%;
    float: left;
}

#plw-oa-transactionfeed .plw-account-list-content h1 {
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    margin-bottom: .5em;
    margin-top: .5em;
    width: 100%;
    font-family:klavika;
}

#plw-oa-transactionfeed .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
}

#plw-oa-transactionfeed .plw-account-list-content div{
  text-align:left;
  margin-bottom:.5em;
}
#plw-oa-transactionfeed .plw-account-list-link{
    padding-top: 1em;
    width: 100%;
    float: right;
    text-align: center;

}

#plw-oa-transactionfeed .plw-account-list-link a{
  color:white;
  background:#0A57A4;
  font-size:16px;
}

/*oa-transactionfeed end*/

@media(min-width: 27em){

/*banner*/
   .plw-img-banner{
    width:25%;
    text-align: center;

   }

   #plw-op-banner .plw-img-banner{
    width:100%;

   }
   

   #plw-hadploan-accountlist .plw-img-banner{
    width:100%;
   }

  #plw-hadploan-accountlist .plw-description-banner{
    width:100%;
  }

   .plw-description-banner{
     width: 100%;
     padding-top:1em;

}

  #plw-op-banner .plw-description-banner{
    width:100%;
    padding-right: 1em;
    padding-left: 1em;
  }

  #plw-op-banner h1{
    text-align:center;
    font-size:18px;
    color:#ffb515;
    margin-top:0;
    margin-bottom:0;
  }

  #plw-op-banner p{
    text-align:center;
    font-size:16px;
  }

  .plw-description-banner h2{
       font-size:17px;

   }

   .plw-description-banner p{
       font-size:15px;

   }


  .plw-link-content-banner{
    padding-top: .5em;
    padding-bottom: 1em;
    width: 100%;
    font-size: 12px;
    font-weight: bold;
  }
  
  #plw-hadploan-accountlist .plw-link-content-banner{
    width:100%;
  }
  #plw-op-banner .plw-link-content-banner{
    font-size:14px;
  }
  
 /*account list */ 
  .plw-account-list-img{
  width: 100%;
}
 .plw-account-list-content {
    width: 100%;
  }
    
.plw-account-list-content p {
    color: #ffB515;
    margin-top: 0;
    margin-bottom: 0;
    padding-top: .5em;
    font-size: 16px;
    text-align: center;
    font-weight: bold;
    font-family: klavika;
}

.plw-account-list-content h1 {
    text-align: center;
    font-family: foro;
    color: white;
    font-weight: normal;
    font-size: 15px;
    margin-top: 0;
    margin-bottom: 0;
}

#plw-hasHadPloan-account-list .plw-account-list-content{

    padding-top: .8em;
    padding-bottom: .5em;
    padding-left: 1em;
    padding-right: 1em;
}

#plw-hasNoPloan-account-list .plw-account-list-content{

    padding-top: .8em;
    padding-bottom: .5em;
    padding-left: 1em;
    padding-right: 1em;
}

.plw-account-list-link {
    padding-top: 0.5em;
    width: 100%;
    color: #ffB515;
    font-weight: bold;
    font-size: 14px;
    text-align: center;
}

#plw-hasHadPloan-account-list .plw-account-list-link {

  padding-bottom:1em;
}
#plw-hasHadPloan-account-list .plw-account-list-link a{
   padding-top: 0.5em;
    background: #ffB515;
    color: black;
    font-weight: bold;
    font-size: 12px;
    text-align: center;
  }

#plw-hasNoPloan-account-list .plw-account-list-link {

  padding-bottom:1em;
}
#plw-hasNoPloan-account-list .plw-account-list-link a{
   padding-top: 0.5em;
    background: #ffB515;
    color: black;
    font-weight: bold;
    font-size: 12px;
    text-align: center;
  }

/*oa-banner satrt*/

#plw-oa-banner{
  width:100%;
}

#plw-oa-banner div{
  font-size:12px;
}
#plw-oa-banner .plw-account-list-content {
    padding-left: .5em;
    width: 60%;
    float: left;
    padding-top:0;
    padding-bottom:0;
}

#plw-oa-banner .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    width: 100%;
    font-family:klavika;
}

#plw-oa-banner .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
}

#plw-oa-banner .plw-account-list-link{
    padding-top: 1em;
    width: 40%;
    float: right;
    text-align: right;
}

#plw-oa-banner .plw-account-list-link a{
  color:white;
  background: #0A57A4;
  font-size: 16px;
}

/*oa -banner-end*/

/*oa-account-list start*/

#plw-oa-accountlist p{
    margin-top: 1em;
    margin-bottom: 1em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
    font-family: arial, sans-serif;
    font-weight: normal;
}

#plw-oa-accountlist div{
  font-size:12px;
}
#plw-oa-accountlist .plw-account-list-content{
  padding-left:.5em;
  padding-right:1em;
  padding-top: .8em;
  padding-bottom: .5em;
  width:100%;
}

#plw-oa-accountlist a{
  font-weight:bold;
  font-size:16px;
}

#plw-oa-accountlist .plw-account-list-link a {
  color:white;
}

#plw-oa-accountlist .plw-account-list-link{
  padding-top:1em;
  width:100%;
}
/*oa-account-list end*/
 /* loan content block*/


.plw-loan-content-img{

  width: 100%;
}

.plw-loan-content-img img{

  max-width: 100%;
  vertical-align: middle;
}

.plw-loan-content-content{
  width:100%;
  text-align:center;
  padding-top: 1em;
  padding-bottom: .5em;
  padding-left: 1em;
  padding-right: 1em;
}

.plw-loan-content-content h1{
    font-family: foro;
    color: white;
    font-weight: normal;
    font-size: 13px;  
    margin-top: 0;
    margin-bottom: 0;
}

.plw-loan-content-content p{
    color: #ffB515;
    margin-top: 0;
    margin-bottom: 0;
    padding-top: 1em;
    font-size: 16px;
    font-weight: bold;
    font-family: klavika;
}

#plw-loan-content-content-text{
  color:white;
  font-weight:normal;
  font-size: 14px;
  padding-top:1em;
  font-family:arial, sans-serif;
}
.plw-loan-content-link{

  padding-top: 1em;
    width: 100%;
    text-align: center;
    font-size:14px;
    padding-right: 1em;
    padding-left: 1em;
    padding-bottom:1em;
}

.plw-loan-content-link a{
  background:#ffB515;
  color:black;
}

/*oa-loancontent satrt*/

#plw-oa-loancontent{
  width:100%;
}

#plw-oa-loancontent div{
  font-size:12px;
}
#plw-oa-loancontent .plw-account-list-content {
    padding-left: .5em;
    padding-right: 1.5em;
    width: 100%;
    float: left;
}

#plw-oa-loancontent .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    width: 100%;
}

#plw-oa-loancontent .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
    font-family: arial, sans-serif;
    font-weight:normal;
}

#plw-oa-loancontent .plw-account-list-link{
    padding-top: 1em;
    padding-left:1em;
    padding-right:1em;
    width: 100%;
    float: right;
    text-align: center;
}

#plw-oa-loancontent .plw-account-list-link a{
  color:white;
  background:#0A57A4;
  font-size: 16px;
}

/*oa-loancontent-end*/

/*op loan content block start*/

.plw-op-loancontent-description{
  width:100%;
  text-align:center;
}

.plw-op-loancontent-description h4{
  padding-top:1em;
  padding-left:1.5em;
  font-family:foro;
  color:white;
  font-weight: normal;
 }
 
 .plw-op-loancontent-description h2{
  padding-top:.5em;
  padding-left:0;
  color:#ffbf00;
 }

 .plw-op-loancontent-link {
  width:100%;
  padding-top: 1em;
  text-align:center;
 }

 .plw-op-loancontent-link span{
  background:#ffbf00;
  color:black;
}

 .plw-op-loancontent-info {
    color: white;
    text-align: left;
    width: 80%;
    text-align:center;
    padding-left: 0.8em;
    border-top: 2px solid grey;
    border-left: none;
    margin-top: 0.4em;
    margin-bottom: 0.6em;
 }
 .plw-op-loancontent-info h4{
  padding-top:.8em;
  font-family:foro;
  color:white;
  font-weight: normal;
 }

.plw-op-loancontent-info-top-pad{
  padding-top:.5em;
}
/*op loan content block end*/

/*transaction history*/
.plw-img-transac-feed{
  
width:0%;
float:left;
  }

.plw-img-transac-feed img{
  max-width:100%;
}

#plw-op-transaction-feed img{
    max-width: 50%;
    padding-top: 0.7em;
    padding-bottom: .5em;
  }

 #plw-op-transaction-feed{
  padding-top: .5em;
  padding-bottom:.5em;
 }
  #plw-op-transaction-feed h1{
    padding-top: 0;
    padding-left:0;
    padding-right:0;
    text-align: center;
    font-size: 18px;
    margin-top:0;
    margin-bottom:0;

  }

  #plw-op-transaction-feed p{
    text-align: center;
    font-size: 16px;
    padding-right:1em;
    color: white;
    font-weight: normal;
    font-family: arial, sans-serif;

  }
   .plw-description-transac-feed{

    width:100%;
    float:left;
  }

.plw-description-transac-feed p{
    color: #ffB515;
    padding-left: 1em;
    padding-top: 1em;
    padding-right: 1em;
    padding-bottom:.5em;
    text-align: center;
    font-size: 20px;
    font-weight:bold;
    font-family:klavika;

}

.plw-link-content-transac-feed{
    float:left;
    padding-top:.5em;
    padding-bottom:1em;
    text-align: center;
    font-size: 16px;
    color: #ffB515;
    font-weight: bold;
    padding-right: 1em;
    width: 100%;

}
 #plw-op-transaction-feed .plw-link-content-transac-feed{

    padding-top: 1.2em;
    padding-right: 1em;
    padding-bottom: 0;
    text-align: center;
    font-size: 18px;
 }

 #plw-op-transaction-feed .plw-link-content-transac-feed{

   padding-top: 1.2em;
    padding-right: 0;
    padding-bottom: .2em;
    text-align: center;
    font-size: 14px;
    padding-right:1em;
 }


/*oa-transactionfeed satrt*/

#plw-oa-transactionfeed{
  width:100%;
}

#plw-oa-transactionfeed div{
  font-size:12px;
}
#plw-oa-transactionfeed .plw-account-list-content {
    padding-left: .5em;
    width: 70%;
    float: left;
    padding-top:0;
    padding-right:1.5em;
}

#plw-oa-transactionfeed .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    width: 100%;
}

#plw-oa-transactionfeed .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
}

#plw-oa-transactionfeed .plw-account-list-link{
    padding-top: 1em;
    width: 30%;
    float: right;
    text-align: left;
}

#plw-oa-transactionfeed .plw-account-list-link a{
  color:white;
  background:#0A57A4;
  font-size:16px;
}

/*oa -transactionfeed-end*/

}

@media(min-width: 40em){

 .plw-container div{
    float: left;
    }
   /*banner */
 
  .plw-img-banner{
    width:20%;
    text-align: center;

   }

   .plw-img-banner img{
    max-width:100%;
   }

   #plw-op-banner .plw-img-banner{
    width:15%;

   }

   #plw-op-banner img{
    width:50%;
    padding-top:.5em;
    padding-bottom:.5em;
   }

   #plw-hadploan-accountlist .plw-img-banner{
    width:35%;
   }

  #plw-hadploan-accountlist .plw-description-banner{
    width:50%;
  }
   .plw-description-banner{
     width: 55%;
     padding-top:.5em;

}
  #plw-op-banner{
    padding-top:.5em;
    padding-bottom:.5em;
  }


  #plw-op-banner .plw-description-banner{
    width:60%;
    padding-top:0;
    padding-bottom:0;
    padding-right:.5em;
    padding-left:.5em;
  }

  #plw-op-banner h1{
    text-align:left;
    font-size:18px;
    color:#ffB515;
    margin-top:0;
    margin-bottom:0;
    padding-top:.5em;
  }

  #plw-op-banner p{
    text-align:left;
    font-size:15px;
  }

  .plw-description-banner h2{
       font-size:17px;

   }

   .plw-description-banner p{
       font-size:15px;

   }


  .plw-link-content-banner{
    padding-top: 1em;
    padding-left:0em;
    width: 25%;
    font-size: 15px;
    font-weight: bold;
  }

  #plw-hadploan-accountlist .plw-link-content-banner{
    padding-top: 0.5em;
    width: 15%;
    font-size: 15px;
    font-weight: bold;

  }
  
  #plw-op-banner .plw-link-content-banner{
    font-size:14px;
    padding-right:1em;
  }


  #plw-hasHadPloan-banner .plw-description-banner {
    text-align:left;
    padding-left:.5em;
    padding-right:.5em;
    padding-top:1em;
    padding-bottom:.5em;
    font-size:1vw;
    

  }
  #plw-hasHadPloan-banner .plw-description-banner p{
    color:#ffB515;
    margin-top:0;
    margin-bottom:0;
    font-size:15px;
    font-weight:bold;
    font-family:klavika;
  }

  #plw-hasNoPloan-banner .plw-description-banner {
    text-align:left;
    padding-left:.5em;
    padding-right:.5em;
    padding-top:1em;
    padding-bottom:.5em;
    font-size:1vw;
    

  }
  #plw-hasNoPloan-banner .plw-description-banner p{
    color:#ffB515;
    margin-top:0;
    margin-bottom:0;
    font-size:15px;
    font-weight:bold;
    font-family:klavika;

  }

  #plw-hasHadPloan-banner .plw-link-content-banner{
    font-size:12px;
    padding-right:1em;
  }

  #plw-hasNoPloan-banner .plw-link-content-banner{
    font-size:12px;
    padding-right:1em;
  }
  
 /*account list */ 
  .plw-account-list-img{
  width: 25%;
  float:left;
}
 .plw-account-list-content {
    width: 55%;
    float:left;
    padding-top:.3em;
    padding-bottom:.5em;
    padding-left:1em;
    padding-right:1em;
  }
    
.plw-account-list-content h1 {
    text-align: left;
    font-family: foro;
    color: white;
    font-weight: normal;
    font-size: 13px;
    margin-top: 0;
    margin-bottom: 0;
}

.plw-account-list-content p {
    color:#ffB515;
    margin-top:0;
    margin-bottom:0;
    padding-top:.5em;
    font-size:13px;
    text-align:left;
    font-weight:bold;
    font-family:klavika;
}


.plw-account-list-link {
    width:20%;
    float:left;
    padding-top:2em;
    padding-right:1em;

  }
.plw-account-list-link a{
    padding-top: 0.5em;
    background:#ffB515;
    color: black;
    font-weight: bold;
    font-size: 12px;
    text-align: center;
}

#plw-hasHadPloan-account-list .plw-account-list-link a{
   padding-top: 0.5em;
    background: #ffB515;
    color: black;
    font-weight: bold;
    font-size: 13px;
    text-align: center;
  }

  #plw-hasNoPloan-account-list .plw-account-list-link a{
   padding-top: 0.5em;
    background: #ffB515;
    color: black;
    font-weight: bold;
    font-size: 13px;
    text-align: center;
  }

/*oa-banner satrt*/

#plw-oa-banner{
  width:100%;
}

#plw-oa-banner div{
  font-size:12px;
}
#plw-oa-banner .plw-account-list-content {
    padding-left: .5em;
    width: 65%;
    float: left;
    padding-top:0;
    padding-bottom:0;
}

#plw-oa-banner .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    margin-top:.5em;
    width: 100%;
}

#plw-oa-banner .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
}

#plw-oa-banner .plw-account-list-link{
    padding-top: 1em;
    width: 35%;
    float: right;
    text-align: left;
}

#plw-oa-banner .plw-account-list-link a{
  color:white;
  background: #0A57A4;
  font-size: 16px;
}
/*oa -banner-end*/

/*oa-account-list start*/

#plw-oa-accountlist{
  width:100%;
}
#plw-oa-accountlist p{
    margin-top: 1em;
    margin-bottom: 1em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
    font-weight: normal;
    font-family: arial, sans-serif;
}

#plw-oa-accountlist div{
  font-size:12px;
  margin-bottom: .5em;
}
#plw-oa-accountlist .plw-account-list-content{
  padding-left:.5em;
  width:80%;
  float:left;
}

#plw-oa-accountlist a{
  font-weight:bold;
  font-size:16px;
}

#plw-oa-accountlist .plw-account-list-link a {
  color:white;
  background: #0A57A4;
}

#plw-oa-accountlist .plw-account-list-link{
  padding-top:1em;
  padding-right: 1em;
  padding-left:1em;
  width:20%;
  float:right;
}
/*op-account-list end*/
 /* loan content block*/


.plw-loan-content-img{

  width: 35%;
}

.plw-loan-content-img img{

  max-width: 100%;
  vertical-align: middle;
}

.plw-loan-content-content{
  width:45%;
  text-align:left;
  padding-top: 1em;
  padding-bottom: .5em;
  padding-left: 1em;
  padding-right: 1em;
}

#plw-hasNoPloan-loan-content .plw-loan-content-content{
 padding-top:.5em;
}

#plw-hasNoPloan-loan-content .plw-loan-content-content p{
 padding-top:.5em;
 margin-top:0;
}

#plw-hasOrHadPloan-loan-content .plw-loan-content-content p{
 margin-top:0;
}

.plw-loan-content-content h1{
    font-family: foro;
    color: white;
    font-weight: normal;
    font-size: 13px;  
    margin-top: 0;
    margin-bottom: 0;
}

.plw-loan-content-content p{
    color: #ffB515;
    margin-top: 0;
    margin-bottom: 0;
    padding-top: 1em;
    font-size: 16px;
    font-weight: bold;
    font-family: klavika;
}

#plw-loan-content-content-text{
  color:white;
  font-weight:normal;
  font-size: 14px;
  padding-top:.5em;
  font-family:arial, sans-serif;
}

.plw-loan-content-link{

  padding-top: 4em;
    width: 20%;
    text-align: center;
    font-size:14px;
    padding-right: 1em;
    padding-left: 1em;
}

.plw-loan-content-link a{
  background:#ffB515;
  color:black;
}
/*oa-loancontent satrt*/

#plw-oa-loancontent{
  width:100%;
}

#plw-oa-loancontent div{
  font-size:12px;
}
#plw-oa-loancontent .plw-account-list-content {
    padding-left: .5em;
    padding-top:0;
    width: 80%;
    float: left;
}

#plw-oa-loancontent .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    margin-top:.5em;
    width: 100%;
}

#plw-oa-loancontent .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
    font-family: arial, sans-serif;
    font-weight:normal;
}

#plw-oa-loancontent .plw-account-list-link{
    padding-top: 2em;
    padding-right: 1em;
    width: 20%;
    float: right;
    text-align: right;
}

#plw-oa-loancontent .plw-account-list-link a{
  color:white;
  background:#0A57A4;
  font-size: 16px;
}

/*oa -loancontent-end*/

/*op loan content block start*/

.plw-op-loancontent-description{
  width:55%;
  text-align:left;
  float:left;
  word-wrap:break-word;
}

.plw-op-loancontent-description h1{
  padding-top:1em;
  padding-left:1.5em;
  font-family:foro;
  color:white;
  font-weight: normal;
  font-size:15px;
  margin-top:0;
  margin-bottom:0;
 }
 
 .plw-op-loancontent-description p{
  padding-top:.5em;
  padding-left:1.5em;
  padding-right:2em;
  color:#ffbf00;
  font-size:16px;
  font-weight: bold;
  font-family:klavika;
 }

 .plw-op-loancontent-link {
  width:17%;
  padding-top: 2em;
  float:left;
 }

 .plw-op-loancontent-link a{
  background:#ffbf00;
  color:black;
}

 .plw-op-loancontent-info {
    color: white;
    text-align: left;
    width: 28%;
    float:left;
    padding-left: 0.8em;
    padding-right:0.8em;
    border-left: 2px solid grey;
    border-top: none;
    margin-top: 0.4em;
    margin-bottom: 0.6em;
 }
 .plw-op-loancontent-info h2{
  padding-top:.6em;
  font-family:foro;
  color:white;
  font-weight: normal;
  font-size: 16px;
 }

.plw-op-loancontent-info-top-pad{
  padding-top:.5em;
}


/*op loan content block end*/

/* Transaction history  */
.plw-img-transac-feed{
  
width:20%;
float:left;
  }

.plw-img-transac-feed img{
  max-width:100%;
  }
  
  #plw-op-transaction-feed img{
   max-width: 80%;
    padding-top: .5em;
  }

  #plw-op-transaction-feed img{
    max-width: 50%;
    padding-top: .7em;
    padding-bottom: .5em;

  }

  #plw-op-transaction-feed{
    padding-top:1em;
    padding-bottom:1em;

  }
  #plw-op-transaction-feed h1{
    padding-top: 0;
    padding-right:0;
    padding-left:0;
    text-align: left;
    font-size: 22px;
    margin-top:0;
    margin-bottom:0;

  }

  #plw-op-transaction-feed p{
    text-align: left;
    padding-right: 1em;
    padding-left: 0;
    padding-top: .5em;
    font-size: 19px;
    color: white;
    font-weight: normal;
    font-family: arial, sans-serif;
  }
  .plw-description-transac-feed{

    width:57%;
    float:left;
  }

.plw-description-transac-feed p{
    color: #ffB515;
    padding-left: .5em;
    padding-top: 1.3em;
    padding-right: .5em;
    padding-bottom:.3em;
    text-align: left;
    font-size: 18px;
    font-weight:bold;
    font-family:klavika;

}

.plw-link-content-transac-feed{
    float:left;
    padding-top:1.5em;
    text-align: right;
    font-size: 16px;
    color: #ffB515;
    font-weight: bold;
    padding-right: 1em;
    width: 23%;

}
 #plw-op-transaction-feed .plw-link-content-transac-feed{

    padding-top: 1.2em;
    padding-right: 1em;
    padding-bottom: 0;
    text-align: center;
    font-size: 18px;
 }

/*oa-banner satrt*/

#plw-oa-transactionfeed{
  width:100%;
}

#plw-oa-transactionfeed div{
  font-size:12px;
}
#plw-oa-transactionfeed .plw-account-list-content {
    padding-left: .5em;
    width: 50%;
    float: left;
    padding-top:0;
}

#plw-oa-transactionfeed .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    margin-top:.5em;
    width: 100%;
}

#plw-oa-transactionfeed .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
}

#plw-oa-transactionfeed .plw-account-list-link{
    padding-top: 1em;
    width: 50%;
    float: right;
    text-align: left;
}

#plw-oa-transactionfeed .plw-account-list-link a{
  color:white;
  background:#0A57A4;
  font-size:16px;
}

}

/*oa -transactionfeed-end*/

@media(min-width: 53em){

  .plw-container div{
    float: left;
    }
   /*banner */
 
  .plw-img-banner{
    width:20%;
    text-align: center;

   }

   #plw-op-banner .plw-img-banner{
    width:15%;

   }

   #plw-op-banner img{
    width:50%;
    padding-top:.5em;
    padding-bottom:.5em;
   }

   .plw-description-banner{
     width: 60%;
     padding-top: 1em;
    padding-left: .5em;
    padding-bottom: 1em;
    padding-right: .5em;

}

  #plw-op-banner .plw-description-banner{
    width:65%;
  }

  #plw-op-banner h1{
    text-align:left;
    padding-top:.5em;
    font-size:20px;
    margin-top:0;
    margin-bottom:0;
    color:#ffB515;
  }

  #plw-op-banner p{
    text-align:left;
    font-size:17px;
  }

  .plw-description-banner h1{
       font-size:19px;

   }

   .plw-description-banner p{
       font-size:17px;

   }


  .plw-link-content-banner{
    padding-top: 1.5em;
    padding-left: 0;
    padding-right: 1em;
    width: 20%;
    font-size: 17px;
    font-weight: bold;
  }
  
  #plw-op-banner .plw-link-content-banner{
    font-size:16px;
  }
  

  #plw-hasHadPloan-banner .plw-description-banner {
    text-align:left;
    padding-left:1em;
    padding-right:1em;
    padding-top:1em;
    padding-bottom:.5em;
    

  }
  #plw-hasHadPloan-banner .plw-description-banner p{
    color:#ffB515;
    margin-top:0;
    margin-bottom:0;
    font-size:17px;
    font-weight:bold;
    font-family:klavika;
  }

  #plw-hasNoPloan-banner .plw-description-banner {
    text-align:left;
    padding-left:1em;
    padding-right:1em;
    padding-top:1em;
    padding-bottom:.5em;
    

  }
  #plw-hasNoPloan-banner .plw-description-banner p{
    color:#ffB515;
    margin-top:0;
    margin-bottom:0;
    font-size:17px;
    font-weight:bold;
    font-family:klavika;
  }

  #plw-hasHadPloan-banner .plw-link-content-banner{
    font-size:15px;
  }

  #plw-hasNoPloan-banner .plw-link-content-banner{
    font-size:15px;
  }

 /*account list */ 
  .plw-account-list-img{
  width: 25%;
  float:left;
}
 .plw-account-list-content {
    width: 55%;
    float:left;
    padding-top:1em;
    padding-bottom:.5em;
    padding-left:1.5em;
    padding-right:1em;
  }
    
.plw-account-list-content h1 {
    text-align: left;
    font-family: foro;
    color: white;
    font-weight: normal;
    font-size: 14px;
    margin-top: 0;
    margin-bottom: 0;
}

.plw-account-list-content p {
    color:#ffB515;
    margin-top:0;
    margin-bottom:0;
    padding-top:1em;
    font-size:16px;
    text-align:left;
    font-weight:bold;
    font-family:klavika;
}


.plw-account-list-link {
    width:20%;
    float:left;
    padding-top:3em;
    padding-right:1em;

  }
.plw-account-list-link a{
    padding-top: 0.5em;
    background:#ffB515;
    color: black;
    font-weight: bold;
    font-size: 14px;
    text-align: center;
}

#plw-hasHadPloan-account-list .plw-account-list-link a{
   padding-top: 0.5em;
    background: #ffB515;
    color: black;
    font-weight: bold;
    font-size: 14px;
    text-align: center;
  }

  #plw-hasNoPloan-account-list .plw-account-list-link a{
   padding-top: 0.5em;
    background: #ffB515;
    color: black;
    font-weight: bold;
    font-size: 14px;
    text-align: center;
  }

/*oa-banner satrt*/

#plw-oa-banner{
  width:100%;
}

#plw-oa-banner div{
  font-size:12px;
}
#plw-oa-banner .plw-account-list-content {
    padding-left: .5em;
    width: 50%;
    float: left;
    padding-top:0;
    padding-bottom:0;
}

#plw-oa-banner .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    margin-top:.5em;
    width: 100%;
}

#plw-oa-banner .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
}

#plw-oa-banner .plw-account-list-link{
    padding-top: 1em;
    width: 50%;
    float: right;
    text-align: left;
}

#plw-oa-banner .plw-account-list-link a{
  color:white;
  background: #0A57A4;
  font-size: 16px;
}

/*oa -banner-end*/

/*oa-account-list start*/

#plw-oa-accountlist{
  width:100%;
}
#plw-oa-accountlist p{
    margin-top: 1em;
    margin-bottom: 1em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
    font-family: arial, sans-serif;
    font-weight: normal;
}

#plw-oa-accountlist div{
  font-size:12px;
}
#plw-oa-accountlist .plw-account-list-content{
  padding-left:.5em;
  width:80%;
  float:left;
}

#plw-oa-accountlist a{
  font-weight:bold;
  font-size:16px;
}

#plw-oa-accountlist .plw-account-list-link a {
  color:white;
  background: #0A57A4;
}

#plw-oa-accountlist .plw-account-list-link{
  padding-top:1em;
  padding-right: 1em;
  padding-left:1em;
  width:20%;
  float:right;
}
/*oa-account-list end*/

 /* loan content block*/


.plw-loan-content-img{

  width: 35%;
}

.plw-loan-content-img img{

  max-width: 100%;
  vertical-align: middle;
}

.plw-loan-content-content{
  width:45%;
  text-align:left;
  padding-top: 1em;
  padding-bottom: .5em;
  padding-left: 1em;
  padding-right: 1em;
}
#plw-hasNoPloan-loan-content .plw-loan-content-content{
 padding-top:1em;
}

#plw-hasNoPloan-loan-content .plw-loan-content-content p{
 padding-top:1em;
}


.plw-loan-content-content h1{
    font-family: foro;
    color: white;
    font-weight: normal;
    font-size: 16px;
    margin-top: 0;
    margin-bottom: 0;
}

.plw-loan-content-content p{
    color: #ffB515;
    margin-top: 0;
    margin-bottom: 0;
    padding-top: 1em;
    font-size: 16px;
    font-weight: bold;
    font-family: klavika;
}

#plw-loan-content-content-text{
  color:white;
  font-weight:normal;
  font-size: 15px;
  padding-top:1em;
  font-family:arial, sans-serif;
}
.plw-loan-content-link{

  padding-top: 4em;
    width: 20%;
    text-align: center;
    font-size:15px;
    padding-right: 1em;
    padding-left: 1em;
}

.plw-loan-content-link a{
  background:#ffB515;
  color:black;

}

/*oa-loancontent satrt*/

#plw-oa-loancontent{
  width:100%;
}

#plw-oa-loancontent div{
  font-size:12px;
}
#plw-oa-loancontent .plw-account-list-content {
    padding-left: .5em;
    padding-top:0;
    width: 80%;
    float: left;
}

#plw-oa-loancontent .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    margin-top:.5em;
    width: 100%;
}

#plw-oa-loancontent .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
    font-family: arial, sans-serif;
    font-weight:normal;
}

#plw-oa-loancontent .plw-account-list-link{
    padding-top: 2em;
    width: 20%;
    float: right;
    text-align: right;
}

#plw-oa-loancontent .plw-account-list-link a{
  color:white;
  background:#0A57A4;
  font-size: 16px;
}

/*oa -loancontent-end*/


/*op loan content block start*/

.plw-op-loancontent-description{
  width:55%;
  text-align:left;
  float:left;
}

.plw-op-loancontent-description h1{
  padding-top:1em;
  padding-left:1.5em;
  font-family:foro;
  color:white;
  font-weight: normal;
  font-size:15px;
  margin-top:0;
  margin-bottom:0;
 }
 
 .plw-op-loancontent-description p{
  padding-top:.5em;
  padding-left:1.5em;
  color:#ffbf00;
  font-size:16px;
  font-weight: bold;
  font-family:klavika;
 }

 .plw-op-loancontent-link {
  width:17%;
  padding-top: 2em;
  float:left;
 }

 .plw-op-loancontent-link a{
  background:#ffbf00;
  color:black;
}

 .plw-op-loancontent-info {
    color: white;
    text-align: left;
    width: 28%;
    float:left;
    padding-left: 0.8em;
    border-left: 2px solid grey;
    border-top: none;
    margin-top: 0.4em;
    margin-bottom: 0.6em;
 }
 .plw-op-loancontent-info h2{
  padding-top:.6em;
  font-family:foro;
  color:white;
  font-weight: normal;
  font-size: 16px;
 }

.plw-op-loancontent-info-top-pad{
  padding-top:.5em;
}


/*op loan content block end*/

/* Transaction history  */
.plw-img-transac-feed{
  
width:20%;
float:left;
  }

#plw-op-transaction-feed .plw-img-transac-feed{
      width:15%;
       
  }
.plw-img-transac-feed img{
  max-width:100%;
}

#plw-op-transaction-feed img{
   max-width: 80%;
    padding-top: .5em;
  }

  #plw-op-transaction-feed img{
    max-width: 60%;
    padding-top: .7em;

  }

  #plw-op-transaction-feed{
    padding-top:.5em;
    padding-bottom:.5em;

  }

  #plw-op-transaction-feed h1{
    padding-top: 0;
    padding-left:0;
    padding-right:0;
    text-align: left;
    font-size: 20px;
    margin-top:0;
    margin-bottom:0;

  }

  #plw-op-transaction-feed p{
    text-align: left;
    padding-left:0;
    padding-right:0;
    font-size: 16px;
    color: white;
    font-weight: normal;
    font-family: arial, sans-serif;

  }

  .plw-description-transac-feed{

    width:57%;
    float:left;
  }

  #plw-op-transaction-feed .plw-description-transac-feed{
    width:60%;
  }
.plw-description-transac-feed p{
    color: #ffB515;
    padding-left: .5em;
    padding-top: .5em;
    padding-right: .5em;
    padding-bottom:.3em;
    text-align: left;
    font-size: 14px;
    font-weight:bold;
    font-family:klavika;

}

.plw-link-content-transac-feed{
    float:left;
    padding-top:1.5em;
    text-align: right;
    font-size: 12px;
    color: #ffB515;
    font-weight: bold;
    padding-right: 1em;
    width: 23%;

}
 #plw-op-transaction-feed .plw-link-content-transac-feed{

   padding-top: 1em;
    padding-bottom: 0;
    padding-right:1em;
    text-align: center;
    font-size: 14px;
    width:25%;
 }

 /*oa-transactionfeed satrt*/

#plw-oa-transactionfeed{
  width:100%;
}

#plw-oa-transactionfeed div{
  font-size:12px;
}
#plw-oa-transactionfeed .plw-account-list-content {
    padding-left: .5em;
    width: 75%;
    float: left;
    padding-top:0;
}

#plw-oa-transactionfeed .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    margin-top:.5em;
    width: 100%;
}

#plw-oa-transactionfeed .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
}

#plw-oa-transactionfeed .plw-account-list-link{
    padding-top: 1em;
    width: 25%;
    float: right;
    text-align: right;
}

#plw-oa-transactionfeed .plw-account-list-link a{
  color:white;
  background:#0A57A4;
  font-size:16px;
}

/*oa -trnsactionfeed-end*/


}


@media(min-width: 66em){
  
 .plw-container div{
    float: left;
    }
   /*banner */
 
  .plw-img-banner{
    width:20%;
    text-align: center;

   }

   #plw-op-banner .plw-img-banner{
    width:15%;

   }

   #plw-op-banner img{
    width:50%;
    padding-top:.5em;
    padding-bottom:.5em;
   }

   .plw-description-banner{
     width: 60%;
     padding-top:1em;

}

#plw-op-banner{
  padding-top:.5em;;
  padding-bottom:.5em;
}
  #plw-op-banner .plw-description-banner{
    width:65%;
    padding-left:.5em;
    padding-right:.5em;
    padding-bottom:0;
  }

  #plw-op-banner h1{
    text-align:left;
    color:#ffb515;
    margin-top:0;
    margin-bottom:0;
  }

  #plw-op-banner p{
    text-align:left;
    font-size:15px;
  }

  .plw-description-banner h1{
       font-size:17px;

   }

   .plw-description-banner p{
       font-size:15px;

   }


  .plw-link-content-banner{
    padding-top: 1.5em;
    padding-left: 0;
    width: 20%;
    font-size: 15px;
    font-weight: bold;
  }
  
  #plw-op-banner .plw-link-content-banner{
    font-size:16px;
    padding-right:1em;
  }
  

  #plw-hasHadPloan-banner .plw-description-banner {
    text-align:left;
    padding-left:1em;
    padding-right:1em;
    padding-top:1em;
    padding-bottom:.5em;
    

  }
  #plw-hasHadPloan-banner .plw-description-banner p{
    color:#ffB515;
    margin-top:0;
    margin-bottom:0;
    font-size:16px;
    font-weight:bold;
    font-family:klavika;
  }

  #plw-hasNoPloan-banner .plw-description-banner {
    text-align:left;
    padding-left:1em;
    padding-right:1em;
    padding-top:1em;
    padding-bottom:.5em;
    

  }
  #plw-hasNoPloan-banner .plw-description-banner p{
    color:#ffB515;
    margin-top:0;
    margin-bottom:0;
    font-size:16px;
    font-weight:bold;
    font-family:klavika;
  }

  #plw-hasHadPloan-banner .plw-link-content-banner{
    font-size:14px;
  }

  #plw-hasNoPloan-banner .plw-link-content-banner{
    font-size:14px;
  }
 /*account list */ 
  .plw-account-list-img{
  width: 25%;
  float:left;
}
 .plw-account-list-content {
    width: 55%;
    float:left;
    padding-top:1em;
    padding-bottom:.5em;
    padding-left:1.5em;
    padding-right:1em;
  }
    
.plw-account-list-content h1 {
    text-align: left;
    font-family: foro;
    color: white;
    font-weight: normal;
    font-size: 14px;
    margin-top: 0;
    margin-bottom: 0;
}

.plw-account-list-content p {
    color:#ffB515;
    margin-top:0;
    margin-bottom:0;
    padding-top:1em;
    font-size:16px;
    text-align:left;
    font-weight:bold;
    font-family:klavika;
}


.plw-account-list-link {
    width:20%;
    float:left;
    padding-top:3em;
    padding-right:1em;

  }
.plw-account-list-link a{
    padding-top: 0.5em;
    background:#ffB515;
    color: black;
    font-weight: bold;
    font-size: 14px;
    text-align: center;
}

/*oa-banner satrt*/

#plw-oa-banner{
  width:100%;
}

#plw-oa-banner div{
  font-size:12px;
}
#plw-oa-banner .plw-account-list-content {
    padding-left: .5em;
    width: 60%;
    float: left;
    padding-top:0;
    padding-bottom:0;
}

#plw-oa-banner .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    margin-top:.5em;
    width: 100%;
}

#plw-oa-banner .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
}

#plw-oa-banner .plw-account-list-link{
    padding-top: 1em;
    width: 40%;
    float: right;
    text-align: left;
}

#plw-oa-banner .plw-account-list-link a{
  color:white;
  background:#0A57A4;
  font-size: 16px;
}

/*oa -banner-end*/

/*oa-account-list start*/

#plw-oa-accountlist{
  width:100%;
}
#plw-oa-accountlist p{
    margin-top: 1em;
    margin-bottom: 1em;
    padding: 0;
    font-size: 14px;
    color: black;
    font-weight: normal;
    text-align: left;
    font-family: arial, sans-serif;
}

#plw-oa-accountlist div{
  font-size:12px;
}
#plw-oa-accountlist .plw-account-list-content{
  padding-left:.5em;
  width:80%;
  float:left;
}

#plw-oa-accountlist a{
  font-weight:bold;
  font-size:16px;
}

#plw-oa-accountlist .plw-account-list-link a {
  color:white;
  background:#0A57A4;
}

#plw-oa-accountlist .plw-account-list-link{
  padding-top:1em;
  padding-right:1em;
  padding-left:1em;
  width:20%;
  float:right;
}
/*oa-account-list end*/
/* loan content block*/

.plw-loan-content-img{

  width: 35%;
}

.plw-loan-content-img img{

  max-width: 100%;
  vertical-align: middle;
}

.plw-loan-content-content{
  width:45%;
  text-align:left;
  padding-top: 1em;
  padding-bottom: .5em;
  padding-left: 1em;
  padding-right: 1em;
}

.plw-loan-content-content h1{
    font-family: foro;
    color: white;
    font-weight: normal;
    font-size: 16px;
    margin-top: 0;
    margin-bottom: 0;
}

.plw-loan-content-content p{
    color: #ffB515;
    margin-top: 0;
    margin-bottom: 0;
    padding-top: 1em;
    font-size: 16px;
    font-weight: bold;
    font-family: klavika;
}

#plw-loan-content-content-text{
  color:white;
  font-weight:normal;
  font-size: 15px;
  padding-top:1em;
  font-family:arial, sans-serif;
}
.plw-loan-content-link{

  padding-top: 4em;
    width: 20%;
    text-align: center;
    font-size:15px;
    padding-right: 1em;
    padding-left: 1em;
}

.plw-loan-content-link a{
  background:#ffB515;
  color:black;
}

/*oa-banner satrt*/

#plw-oa-loancontent{
  width:100%;
}

#plw-oa-loancontent div{
  font-size:12px;
}
#plw-oa-loancontent .plw-account-list-content {
    padding-left: .5em;
    padding-top:0;
    width: 80%;
    float: left;
}

#plw-oa-loancontent .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 18px;
    font-weight: bold;
    padding-top: 0em;
    margin-top:.5em;
    width: 100%;
}

#plw-oa-loancontent .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
    font-family: arial, sans-serif;
    font-weight:normal;
}

#plw-oa-loancontent .plw-account-list-link{
    padding-top: 2em;
    width: 20%;
    float: right;
    text-align: right;
}

#plw-oa-loancontent .plw-account-list-link a{
  color:white;
  background:#0A57A4;
  font-size: 16px;
}

/*oa -loancontent-end*/
/*op loan content block start*/

.plw-op-loancontent-description{
  width:55%;
  text-align:left;
  float:left;
  font-size:16px;
}

.plw-op-loancontent-description h1 {
  padding-top:1em;
  padding-left:1.5em;
  font-family:foro;
  color:white;
  font-weight: normal;
  font-size:16px;
  margin-top:0;
  margin-bottom:0;
 }
 
 .plw-op-loancontent-description p{
  padding-top:.5em;
  padding-left:1.5em;
  color:#ffbf00;
  font-size:16px;
  font-weight: bold;
  font-family:klavika;
 }

 .plw-op-loancontent-link {
  width:17%;
  padding-top: 2em;
  float:left;
 }

 .plw-op-loancontent-link a{
  background:#ffbf00;
  color:black;
}

 .plw-op-loancontent-info {
    color: white;
    text-align: left;
    width: 28%;
    float:left;
    padding-left: 0.8em;
    border-left: 2px solid grey;
    border-top: none;
    margin-top: 0.4em;
    margin-bottom: 0.6em;
 }
 .plw-op-loancontent-info h2{
  padding-top:.6em;
  font-family:foro;
  color:white;
  font-weight: normal;
  font-size: 16px;
 }

.plw-op-loancontent-info-top-pad{
  padding-top:.5em;
}

/*op loan content block end*/

/* Transaction history  */
.plw-img-transac-feed{
  
  width:20%;
  float:left;
  } 

.plw-img-transac-feed img{
  max-width:100%;
}
 
 #plw-op-transaction-feed .plw-img-transac-feed{
      width:15%;
       
  }
  #plw-op-transaction-feed img{
   max-width: 80%;
    padding-top: .5em;
  }

  #plw-op-transaction-feed img{
    max-width: 60%;
    padding-top: .7em;

  }

  #plw-op-transaction-feed{
    padding-top:.5em;
    padding-bottom:.5em;
  }
  #plw-op-transaction-feed h1{
    padding-left:0;
    padding-top: 0;
    padding-right:0;
    text-align: left;
    font-size: 16px;
    margin-top:0;
    margin-bottom:0;

  }

  #plw-op-transaction-feed p{
    text-align: left;
    font-size: 13px;
    padding-right:0;
    color: white;
    font-weight: normal;
    font-family: arial, sans-serif;

  }

  .plw-description-transac-feed{

    width:57%;
    float:left;
  }

  #plw-op-transaction-feed .plw-description-transac-feed{
    width:60%;
  }

.plw-description-transac-feed p{
    color: #ffB515;
    padding-left: .5em;
    padding-top: .5em;
    padding-right: .5em;
    padding-bottom:.3em;
    text-align: left;
    font-size: 14px;
    font-weight:bold;
    font-family:klavika;

}

.plw-link-content-transac-feed{
    float:left;
    padding-top:1.5em;
    text-align: right;
    font-size: 12px;
    color: #ffB515;
    font-weight: bold;
    padding-right: 1em;
    width: 23%;

}

 #plw-op-transaction-feed .plw-link-content-transac-feed{

    padding-top: 1em;
    padding-bottom: 0;
    padding-right:1em;
    text-align: center;
    font-size: 13px;
    width:25%;
 }
/*oa-transactionfeed satrt*/

#plw-oa-transactionfeed{
  width:100%;
}

#plw-oa-transactionfeed div{
  font-size:12px;
}
#plw-oa-transactionfeed .plw-account-list-content {
    padding-left: .5em;
    width: 75%;
    float: left;
    padding-top:0;
}

#plw-oa-transactionfeed .plw-account-list-content h1{
    color: black;
    text-align: left;
    font-size: 16px;
    font-weight: bold;
    padding-top: 0em;
    margin-top:.5em;
    width: 100%;
}

#plw-oa-transactionfeed .plw-account-list-content p{
    margin-top: .5em;
    margin-bottom: .5em;
    padding: 0;
    font-size: 14px;
    color: black;
    text-align: left;
}

#plw-oa-transactionfeed .plw-account-list-link{
    padding-top: 1em;
    width: 25%;
    float: right;
    text-align: right;
}

#plw-oa-transactionfeed .plw-account-list-link a{
  color:white;
  background:#0A57A4;
  font-size: 16px;
}

/*oa -transactionfeed-end*/

}
  

  





`);   
    
}