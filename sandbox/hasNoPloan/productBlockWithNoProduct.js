module.exports = function(resCon){
    
    return `<li id='plw-hasNoPloan-withNoLoanAccounts' class='component tableRow_std_actionCard_summaryStatus'>
                       <section class='plw-container clearfix' id = 'plw-hasNoPloan-loan-content'>
        <div class='plw-loan-content-img' id='plw-hasNoPloan-content-img'>
            <img src='https://s3.us-east-2.amazonaws.com/open-opportunity-new-design-customer-1/images/image+2.png' alt=''>
        </div>
        <div class='plw-loan-content-content'  id='plw-hasNoPloan-content-content'>
            <h1>
                PERSONAL LOANS
            </h1>
        <p> From the unexpected to the long-awaited, a Personal Loan that fits every plan.  </p>
        <p id='plw-loan-content-content-text'> See how fast and easy it is to apply.</p>
    </div>
            <div class= 'plw-loan-content-link'  id='plw-hasNoPloan-content-link'>
                
<a class=' component button_link_std_primary wbst_fnc__linkButton  ' id='plw-hasNoPloan-content-link-Button' href='${process.env.hasNoPloanURL}' target='_blank' data-mm-is-active='true' data-mm-component='button' data-mm-skin='primary' data-warp-validation='validated' data-wbst-inject='hidden:hidden href:href text:text aria-label:label target:target' data-mm-subclass='link' data-mm-template='std'>Learn more</a>
        </div>  

    </section>
                            
                    </li>`;
};




    