module.exports = function(resCon){
    
    return `<li id='plw-hasOrHadPloan-withNoLoanAccounts' class='component tableRow_std_actionCard_summaryStatus'>
        <section class='plw-container clearfix' id = 'plw-hasHadPloan-loan-content'>
        <div class='plw-loan-content-img' id = 'plw-hasHadPloan-loan-content-img'>
            <img src='https://s3.us-east-2.amazonaws.com/open-opportunity-new-design-customer-1/images/image+6.png' alt=''>
        </div>
        <div class='plw-loan-content-content' id = 'plw-hasHadPloan-loan-content-content'>
            <h1>
                PERSONAL LOANS
            </h1>
        <p> A Personal Loan that can keep up with your personal life.</p>
        <p id='plw-loan-content-content-text'> See how fast and easy it is to apply.</p>
    </div>
            <div class= 'plw-loan-content-link' id = 'plw-hasHadPloan-loan-content-link'>
                
<a class=' component button_link_std_primary wbst_fnc__linkButton  ' id='plw-hasHadPloan-loan-content-link-Button' href='${process.env.hasOrHadPloanURL}' target='_blank' data-mm-is-active='true' data-mm-component='button' data-mm-skin='primary' data-warp-validation='validated' data-wbst-inject='hidden:hidden href:href text:text aria-label:label target:target' data-mm-subclass='link' data-mm-template='std'>Learn more</a>
        </div>  

    </section>
                            
                    </li>`;
};




    