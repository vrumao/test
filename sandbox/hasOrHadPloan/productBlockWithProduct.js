
module.exports = function (response){
    let repeatableContent = ``;
    response.forEach((datum, index)=>{
        repeatableContent+=getLoanRepeatableContent(datum, index);
    });
    let loadTemplate =  `
            <section id='plw-hasOrHadPloan-CustomerSegment_details' class='component formSection_table_summaryList_std'>
                <ul id='plw-hasOrHadPloan-CustomerSegment_details__body' class='body'>
                    ${repeatableContent}            
                </ul>
            </section>
            `;
    return loadTemplate;
};
function getLoanRepeatableContent(resCon){
    return `<li id='plw-hasOrHadPloan-withLoanAccounts' class='component tableRow_std_actionCard_summaryStatus' >
    <section class='plw-container clearfix' id='plw-hasHadPloan-account-list'>
        <div class='plw-account-list-img' id='plw-hasHadPloan-account-list-img'>
            <img src='https://s3.us-east-2.amazonaws.com/open-opportunity-new-design-customer-1/images/image+5.png' alt=''>
        </div>
        <div class='plw-account-list-content' id='plw-hasHadPloan-account-list-content'>
            <h1>
                PERSONAL LOANS
            </h1>
        <p> Fast Funding that keeps up with your to-do-list. </p>
       </div>
       <div class= 'plw-account-list-link' id='plw-hasHadPloan-account-list-link'>   
              <a class=' component button_link_std_primary wbst_fnc__linkButton  ' id='plw-hasHadPloan-account-list-link-Button' href='${process.env.hasOrHadPloanURL}' target='_blank' data-mm-is-active='true' data-mm-component='button' data-mm-skin='primary' data-warp-validation='validated' data-wbst-inject='hidden:hidden href:href text:text aria-label:label target:target' data-mm-subclass='link' data-mm-template='std'>Learn more</a>
            </div>
    </section>


                        
                    </li>`;
    
}












