module.exports = function(resCon)
{
    return `<section  id = 'plw-hasOrHadPloan-transactionHistoryAdBanner'>
            
            <a href='${process.env.hasOrHadPloanURL}' id='plw-hasOrHadPloan-transactionHistoryAdBanner-link'>
    <div class='plw-container clearfix'  id='plw-hasHadPloan-transaction-feed'>
            <div class='plw-img-transac-feed' id='plw-hasHadPloan-transaction-feed-img'>
                <img src='https://s3.us-east-2.amazonaws.com/open-opportunity-new-design-customer-1/images/image+7.png' alt=''>
            </div>
            <div class = 'plw-description-transac-feed' id='plw-hasHadPloan-transaction-feed-content'>
            
               <p>
                By the time you finish your coffee, you could finish a Personal Loan application.
               </p>
             
            </div>
            <div class='plw-link-content-transac-feed' id='plw-hasHadPloan-transaction-feed-link'>
               <span class=' component button_link_std_primary wbst_fnc__linkButton  ' id='plw-hasHadPloan-transaction-feed-link-Button'  data-mm-is-active='true' data-mm-component='button' data-mm-skin='primary' data-warp-validation='validated' data-wbst-inject='hidden:hidden href:href text:text aria-label:label target:target' data-mm-subclass='link' data-mm-template='std'>Start here</span>
            </div>
        </div>
        </a>
    
        
         </section>`;
}



 
    