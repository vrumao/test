module.exports = function (response){
    return `<section id = 'plw-hasOrHadPloan-summaryAdBanner'>
    
    
    <a href='${process.env.hasOrHadPloanURL}' id='plw-hasOrHadPloan-summaryAdBanner-link'>

<div class='plw-container clearfix'  id='plw-hasHadPloan-banner'>
        <div class='plw-img-banner' id='plw-hasHadPloan-banner-img'>
            <img src='https://s3.us-east-2.amazonaws.com/open-opportunity-new-design-customer-1/images/image+3.png' alt=''>
        </div>
        <div class='plw-description-banner' id='plw-hasHadPloan-banner-content'>
            <p>
                A personal Loan application that's Faster than your morning commute.
            </p>
        </div>
        <div class='plw-link-content-banner' id='plw-hasHadPloan-banner-link'>
            <span class=' component button_link_std_primary wbst_fnc__linkButton  ' id='plw-hasHadPloan-banner-link-Button'  data-mm-is-active='true' data-mm-component='button' data-mm-skin='primary' data-warp-validation='validated' data-wbst-inject='hidden:hidden href:href text:text aria-label:label target:target' data-mm-subclass='link' data-mm-template='std'>Learn more</span>
        </div>
    </div>

        </a>
    
       </section>
       `;
}