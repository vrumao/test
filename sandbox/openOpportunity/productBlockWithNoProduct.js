module.exports = function(resCon,){
    
    return `<li  id='plw-openOpportunity-withNoLoanAccounts' class='component tableRow_std_actionCard_summaryStatus'>
        <section class='plw-container clearfix ' id='plw-op-loancontent'>
        
        <div class='plw-op-loancontent-description'  id='plw-op-withNoLoanAccounts-description'>
        <h1>
            PERSONAL LOAN
        </h1>
        <p>
        By the time you finish your coffee, you could finish a Personal Loan application.
       </p>
    </div>
        <div class= 'plw-op-loancontent-link' id='plw-op-withNoLoanAccounts-link'>           
            <a class=' component button_link_std_primary wbst_fnc__linkButton  ' id='plw-op-withNoLoanAccounts-link-Button' href='${process.env.opURL}${resCon.Id}' target='_blank' data-mm-is-active='true' data-mm-component='button' data-mm-skin='primary' data-warp-validation='validated' data-wbst-inject='hidden:hidden href:href text:text aria-label:label target:target' data-mm-subclass='link' data-mm-template='std' >Continue</a>
        </div>

        <div class='plw-op-loancontent-info' id='plw-op-withNoLoanAccounts-bankerInfo'>
       <h2>BANKER INFO</h2>
       <p class='plw-op-loancontent-info-top-pad'> ${resCon.Owner.Name}</p>
       <p>${resCon.Owner.Phone}</p>
       <p>${resCon.Owner.Email}</p>
    </div>

    </section>
                            
                    </li>`;
};




    