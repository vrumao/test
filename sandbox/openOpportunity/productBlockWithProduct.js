//module.exports = function getLoanTemplate(response)
module.exports = function (response){
    let repeatableContent = ``;
    response.forEach((datum)=>{
        repeatableContent+=getLoanRepeatableContent(datum);
    });
    let loadTemplate =  `
            <section id='plw-openOpportunity-Opportunity_details' class='component formSection_table_summaryList_std'>
                <ul id='plw-openOpportunity-Opportunity_details__body' class='body'>
                    ${repeatableContent}            
                </ul>
            </section>
            `;
    return loadTemplate;
};
function getLoanRepeatableContent(resCon){
    return `<li id='plw-openOpportunity-withLoanAccounts' class='component tableRow_std_actionCard_summaryStatus' >
    <section class='plw-container clearfix ' id='plw-op-loancontent'>
        
        <div class='plw-op-loancontent-description'  id='plw-op-withLoanAccounts-description'>
        <h1>
            PERSONAL LOAN
        </h1>
        <p>
        By the time you finish your coffee, you could finish a Personal Loan application.
       </p>
    </div>
        <div class= 'plw-op-loancontent-link' id='plw-op-withLoanAccounts-link'>           
            <a class=' component button_link_std_primary wbst_fnc__linkButton  ' id='plw-op-withLoanAccounts-link-Button' href='${process.env.opURL}${resCon.Id}' target='_blank' data-mm-is-active='true' data-mm-component='button' data-mm-skin='primary' data-warp-validation='validated' data-wbst-inject='hidden:hidden href:href text:text aria-label:label target:target' data-mm-subclass='link' data-mm-template='std' >Continue</a>
        </div>

        <div class='plw-op-loancontent-info' id='plw-op-withLoanAccounts-bankerInfo'>
       <h2>BANKER INFO</h2>
       <p class='plw-op-loancontent-info-top-pad'> ${resCon.Owner.Name}</p>
       <p>${resCon.Owner.Phone}</p>
       <p>${resCon.Owner.Email}</p>
    </div>

    </section>
                        
                    </li>`;
    
}












