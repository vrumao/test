
module.exports = function (resCon){
   
    return `<section id = 'plw-openOpportunity-summaryAdBanner'>
     <a href='${process.env.opURL}${resCon.Id}' id='plw-op-bannerlink' >
            
    <div class='plw-container clearfix' id='plw-op-banner'>
        <div class='plw-img-banner' id='plw-op-banner-img'>
            <img src='https://s3.us-east-2.amazonaws.com/open-opportunity-new-design-customer-1/images/checklist+icon.png'  alt=''>
        </div>
        <div class='plw-description-banner' id='plw-op-banner-content'>
            <h1>
                Got a minute?
            </h1>
            <p>${resCon.Owner.Name.split(' ')[0]} started your Personal Loan app. Complete it now.</p>
        </div>
        <div class='plw-link-content-banner' id='plw-op-banner-link'>
            <span class=' component button_link_std_primary wbst_fnc__linkButton  ' id='plw-op-banner-link-Button' data-mm-is-active='true' data-mm-component='button' data-mm-skin='primary' data-warp-validation='validated' data-wbst-inject='hidden:hidden href:href text:text aria-label:label target:target' data-mm-subclass='link' data-mm-template='std'>Start here</span>
                  </div>
    </div>

        </a>
       </section>
       `;
};


