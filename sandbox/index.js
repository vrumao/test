console.log('GOOD JOB');

const AWS = require("aws-sdk");
AWS.config.update({
    region: process.env.REGION_NAME,
    endpoint: process.env.END_POINT_URL
});
var dbClient = new AWS.DynamoDB.DocumentClient({
    api_version: '2012-08-10'
});

const getStyleTemplate = require('./style');

function calculateCIF(cif) {
    var x = 26 - cif.length;
    var padding = '0';
    var new_cif = `0026` + padding.repeat(x) + cif;
    return new_cif;

}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

function errorHandler(code, err,context) {
    return {
        errorType: "InternalServerError",
        httpStatus: code,
        requestId: context.awsRequestId,
        trace: { err }
    };
}

exports.handler = (event, context, callback) => {

    //console.log("----", event['pathParameters']['customercif']);
    //console.log("----", JSON.stringify(event);
    let customerCIF = event.customercif;

    if (customerCIF.length < 30) {
        customerCIF = calculateCIF(customerCIF);
    }


    let params = {
        TableName: process.env.application,
        KeyConditionExpression: "CIF = :cif",
        ExpressionAttributeValues: {
            ':cif': customerCIF
        }
    };
    
    let responseBody = {
        "statusCode": 200,
        "headers": {
            "my_header": "my_value"
        },
        "body": "",
        "isBase64Encoded": false
    };

    dbClient.query(params, function (err, applicationResponse) {
        if (err) {
            console.log('An error has occured while calling db');
            var myErrorObj = errorHandler(500, err, context);
            return callback(JSON.stringify(myErrorObj), null);
        }

        params.TableName = process.env.opportunity;
        dbClient.query(params, function (err, opportunityResponse) {
            if (err) {
                console.log('An error has occured while calling db');
                 myErrorObj = errorHandler(500, err, context);
                return callback(JSON.stringify(myErrorObj), null);
            }

            params.TableName = process.env.customerSegment;
            dbClient.query(params, function (err, customerSegmentResponse) {
                if (err) {
                    console.log('An error has occured while calling db');
                    myErrorObj = errorHandler(500, err, context);
                    return callback(JSON.stringify(myErrorObj), null);
                }
                //console.log("response", JSON.stringify(response.Items));
                responseBody.body = `
                    (function() {
                        
                        var head = document.head || document.getElementsByTagName('head')[0],
                        styleElement = document.createElement('style');
                    
                        styleElement.type = 'text/css';
                        styleElement.appendChild(document.createTextNode(${getStyleTemplate()}));
                    
                        head.appendChild(styleElement); `;
                let applicationResponseResults = applicationResponse.Items || [];
                let applicationSingleDatum = applicationResponseResults[0] || {};

                let opportunityResponseResults = opportunityResponse.Items || [];
                let opportunitySingleDatum = opportunityResponseResults[0] || {};
                
    
                let customerSegmentResults = customerSegmentResponse.Items || [];
                let customerSegmentSingleDatum = customerSegmentResults[0] || {};

                console.log("applicationResponse", JSON.stringify(applicationResponse));
                console.log("opportunityResponse", JSON.stringify(opportunityResponse));
                console.log("CustomerSegmentResponse", JSON.stringify(customerSegmentResponse));
                if (applicationResponseResults.length) {

                    const oa_productBlockWithProduct = require('./openApplication/productBlockWithProduct');
                    const oa_transactionHistoryBanner = require('./openApplication/transactionHistoryBanner');
                    const oa_summaryBanner = require('./openApplication/summaryBanner');
                    const oa_productBlockWithNoProduct = require('./openApplication/productBlockWithNoProduct');

                    responseBody.body += ` if (document.getElementById('loan_accounts__section-note') != null 
                                && (document.getElementById('loan_accounts.non-product-owner-content') == null)) {
                                    var hasLoanBlock = document.getElementById('loan_accounts__section-note'); hasLoanBlock.parentElement.insertBefore(hasLoanBlock , hasLoanBlock.parentElement.children[2]);
                                    hasLoanBlock.innerHTML = "${oa_productBlockWithProduct(applicationResponseResults)}"; 
                                }
                                else if (document.getElementById('loan_accounts__section-note') != null 
                                        && document.getElementById('loan_accounts.non-product-owner-content') != null) { document.getElementById('loan_accounts__body').style.display = 'none';
                                    var noLoanBlock = document.getElementById('loan_accounts__section-note');
                                    noLoanBlock.innerHTML = "${oa_productBlockWithNoProduct(applicationSingleDatum)}";
                                }
                            	if(document.getElementById('summary_adBanner') != null) {
                                	var bannerBlock = document.getElementById('summary_adBanner');
                        	        bannerBlock.removeChild(bannerBlock.childNodes[1]);
                                    bannerBlock.innerHTML = "${oa_summaryBanner(applicationSingleDatum)}";
                            	}
                                if (document.getElementById('adBanner') != null) {
                                    var navBlock = document.getElementById('adBanner');
                                    navBlock.innerHTML = "${oa_transactionHistoryBanner(applicationSingleDatum)}";
                                }`;
                } else if (opportunityResponseResults.length) {

                    const op_productBlockWithProduct = require('./openOpportunity/productBlockWithProduct');
                    const op_transactionHistoryBanner = require('./openOpportunity/transactionHistoryBanner');
                    const op_summaryBanner = require('./openOpportunity/summaryBanner');
                    const op_productBlockWithNoProduct = require('./openOpportunity/productBlockWithNoProduct');

                    responseBody.body += ` if (document.getElementById('loan_accounts__section-note') != null 
                                && (document.getElementById('loan_accounts.non-product-owner-content') == null)) {
                                    var hasLoanBlock = document.getElementById('loan_accounts__section-note'); hasLoanBlock.parentElement.insertBefore(hasLoanBlock , hasLoanBlock.parentElement.children[2]);
                                    hasLoanBlock.innerHTML = "${op_productBlockWithProduct(opportunityResponseResults)}"; 
                                }
                                else if (document.getElementById('loan_accounts__section-note') != null 
                                        && document.getElementById('loan_accounts.non-product-owner-content') != null) { document.getElementById('loan_accounts__body').style.display = 'none';
                                    var noLoanBlock = document.getElementById('loan_accounts__section-note');
                                    noLoanBlock.innerHTML = "${op_productBlockWithNoProduct(opportunitySingleDatum)}";
                                }
                            	if(document.getElementById('summary_adBanner') != null) {
                                	var bannerBlock = document.getElementById('summary_adBanner');
                        	        bannerBlock.removeChild(bannerBlock.childNodes[1]);
                                    bannerBlock.innerHTML = "${op_summaryBanner(opportunitySingleDatum)}";
                            	}
                                if (document.getElementById('adBanner') != null) {
                                    var navBlock = document.getElementById('adBanner');
                                    navBlock.innerHTML = "${op_transactionHistoryBanner(opportunitySingleDatum)}";
                                }`;
                } else if (customerSegmentResults.length) {

                    switch (customerSegmentSingleDatum['segmentName']) {

                        case 'hasNoPloan':
                            const hasNoPloan_productBlockWithProduct = require('./hasNoPloan/productBlockWithProduct');
                            const hasNoPloan_transactionHistoryBanner = require('./hasNoPloan/transactionHistoryBanner');
                            const hasNoPloan_summaryBanner = require('./hasNoPloan/summaryBanner');
                            const hasNoPloan_productBlockWithNoProduct = require('./hasNoPloan/productBlockWithNoProduct');

                            responseBody.body += `
                                if (document.getElementById('loan_accounts__section-note') != null 
                                && (document.getElementById('loan_accounts.non-product-owner-content') == null)) {
                                    var hasLoanBlock = document.getElementById('loan_accounts__section-note'); hasLoanBlock.parentElement.insertBefore(hasLoanBlock , hasLoanBlock.parentElement.children[2]);
                                    hasLoanBlock.innerHTML = "${hasNoPloan_productBlockWithProduct(customerSegmentResults)}"; 
                                }
                                else if (document.getElementById('loan_accounts__section-note') != null 
                                        && document.getElementById('loan_accounts.non-product-owner-content') != null) { document.getElementById('loan_accounts__body').style.display = 'none';
                                    var noLoanBlock = document.getElementById('loan_accounts__section-note');
                                    noLoanBlock.innerHTML = "${hasNoPloan_productBlockWithNoProduct(customerSegmentSingleDatum)}";
                                }
                            	if(document.getElementById('summary_adBanner') != null) {
                                	var bannerBlock = document.getElementById('summary_adBanner');
                        	        bannerBlock.removeChild(bannerBlock.childNodes[1]);
                                    bannerBlock.innerHTML = "${hasNoPloan_summaryBanner()}";
                            	}
                                if (document.getElementById('adBanner') != null) {
                                    var navBlock = document.getElementById('adBanner');
                                    navBlock.innerHTML = "${hasNoPloan_transactionHistoryBanner()}";
                                }`;

                            break;

                        case 'hasOrHadPloan':
                            const hasOrHadPloan_productBlockWithProduct = require('./hasOrHadPloan/productBlockWithProduct');
                            const hasOrHadPloan_transactionHistoryBanner = require('./hasOrHadPloan/transactionHistoryBanner');
                            const hasOrHadPloan_summaryBanner = require('./hasOrHadPloan/summaryBanner');
                            const hasOrHadPloan_productBlockWithNoProduct = require('./hasOrHadPloan/productBlockWithNoProduct');

                            responseBody.body += `
                                if (document.getElementById('loan_accounts__section-note') != null 
                                && (document.getElementById('loan_accounts.non-product-owner-content') == null)) {
                                    var hasLoanBlock = document.getElementById('loan_accounts__section-note'); hasLoanBlock.parentElement.insertBefore(hasLoanBlock , hasLoanBlock.parentElement.children[2]);
                                    hasLoanBlock.innerHTML = "${hasOrHadPloan_productBlockWithProduct(customerSegmentResults)}"; 
                                }
                                else if (document.getElementById('loan_accounts__section-note') != null 
                                        && document.getElementById('loan_accounts.non-product-owner-content') != null) { document.getElementById('loan_accounts__body').style.display = 'none';
                                    var noLoanBlock = document.getElementById('loan_accounts__section-note');
                                    noLoanBlock.innerHTML = "${hasOrHadPloan_productBlockWithNoProduct(customerSegmentSingleDatum)}";
                                }
                            	if(document.getElementById('summary_adBanner') != null) {
                                	var bannerBlock = document.getElementById('summary_adBanner');
                        	        bannerBlock.removeChild(bannerBlock.childNodes[1]);
                                    bannerBlock.innerHTML = "${hasOrHadPloan_summaryBanner()}";
                            	}
                                if (document.getElementById('adBanner') != null) {
                                    var navBlock = document.getElementById('adBanner');
                                    navBlock.innerHTML = "${hasOrHadPloan_transactionHistoryBanner()}";
                                }
                                
                                `;
                            break;


                    }
                }

                

                if (isEmpty(opportunitySingleDatum) && isEmpty(applicationSingleDatum) && isEmpty(customerSegmentSingleDatum) ) {
                    console.log('Cif is not present in the db');
                    err = {"message":"Invalid Cif"};
                    myErrorObj = errorHandler(404, err, context);
                    return callback(JSON.stringify(myErrorObj), null);
                }

                responseBody.body += `})();`;
                responseBody.body = responseBody.body.replace(/\n|\r|\t/g, " ");
                
                console.log(context);
                callback(null, responseBody.body);
            });
        });
    });


};
