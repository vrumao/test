module.exports = function(resCon,){
    
    return `<li  id='plw-openApplication-withNoLoanAccounts' class='component tableRow_std_actionCard_summaryStatus'>
<section class=' component message_std_warning_std wbst_fnc__message_std_warning_std1  ' id='plw-oa-loancontent' data-mm-skin-subset='max_16' data-mm-component='message' data-mm-skin='std' data-warp-validation='validated' data-wbst-inject='hidden:hidden' data-mm-subclass='std' data-mm-template='warning' data-mm-text-key='component-library.message.std.warning.std1.body' data-mm-icon-text-key='component-library.message.std.warning.std1'>

<div class='plw-account-list-content' id='plw-oa-withNoLoanAccounts-description'>

<h1> It's time to sign your loan documents.</h1>

<P> Your loan is ready to close. Once you sign your documents, we'll be ready to fund your account.</P>

    <span class=' icon-wrapper' id='plw-oa-loancontent-icon-wrapper' data-wbst-inject='hidden:hidden'>
        <i class=' icon' id='plw-oa-loancontent-icon' data-wbst-inject='hidden:hidden'></i>
        <span class=' icon-text' id='plw-oa-loancontent-icon-text' data-wbst-inject='hidden:hidden text:iconText'></span>
    </span>
    <div class=' body' id='plw-oa-loancontent-status' data-wbst-inject='hidden:hidden html:text'>Loan Status: ${resCon.stageName}.</div>
    </div>

    <div class='plw-account-list-link' id='plw-oa-withNoLoanAccounts-link'>

<a class=' component button_link_std_primary wbst_fnc__linkButton  ' id='plw-oa-withNoLoanAccounts-link-Button' href='${process.env.oaURL}' target='_blank' data-mm-is-active='true' data-mm-component='button' data-mm-skin='primary' data-warp-validation='validated' data-wbst-inject='hidden:hidden href:href text:text aria-label:label target:target' data-mm-subclass='link' data-mm-template='std' data-mm-text-key='cl.button.link.std.primary.text'>Continue</a>


    </div>

</section>
                            
                    </li>`;
};




    