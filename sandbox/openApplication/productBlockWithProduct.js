//module.exports = function getLoanTemplate(response)
module.exports = function (response){
    let repeatableContent = ``;
    response.forEach((datum)=>{
        repeatableContent+=getLoanRepeatableContent(datum);
    });
    let loadTemplate =  `
            <section id='plw-openApplication-Opportunity_details' class='component formSection_table_summaryList_std'>
                <ul id='plw-openApplication-Opportunity_details__body' class='body'>
                    ${repeatableContent}            
                </ul>
            </section>
            `;
    return loadTemplate;
};
function getLoanRepeatableContent(resCon){
    return `<li id='plw-openApplication-withLoanAccounts' class='component tableRow_std_actionCard_summaryStatus' >
    <section class=' component message_std_warning_std wbst_fnc__message_std_warning_std1  ' id='plw-oa-accountlist' data-mm-skin-subset='max_16' data-mm-component='message' data-mm-skin='std' data-warp-validation='validated' data-wbst-inject='hidden:hidden' data-mm-subclass='std' data-mm-template='warning' data-mm-text-key='component-library.message.std.warning.std1.body' data-mm-icon-text-key='component-library.message.std.warning.std1'>

<div class='plw-account-list-content' id='plw-oa-withLoanAccounts-description'>

<a href='${process.env.oaURL}'> Personal Loan Application</a>

<P> Your loan is ready to close. Once you sign your documents, we'll be ready to fund your account.</P>

    <span class=' icon-wrapper' id='plw-oa-accountlist-icon-wrapper' data-wbst-inject='hidden:hidden'>
        <i class=' icon' id='plw-oa-accountlist-icon' data-wbst-inject='hidden:hidden'></i>
        <span class=' icon-text' id='plw-oa-accountlist-icon-text' data-wbst-inject='hidden:hidden text:iconText'></span>
    </span>
    <div class=' body' id='plw-oa-accountlist-status' data-wbst-inject='hidden:hidden html:text'>Loan status: ${resCon.stageName}</div>
    </div>

    <div class='plw-account-list-link' id='plw-oa-withLoanAccounts-link'>

<a class=' component button_link_std_primary wbst_fnc__linkButton  ' id='plw-oa-withLoanAccounts-link-Button' href='${process.env.oaURL}' target='_blank' data-mm-is-active='true' data-mm-component='button' data-mm-skin='primary' data-warp-validation='validated' data-wbst-inject='hidden:hidden href:href text:text aria-label:label target:target' data-mm-subclass='link' data-mm-template='std' data-mm-text-key='cl.button.link.std.primary.text'>Continue</a>


    </div>

</section>

                        
                    </li>`;
    
}












